#!/bin/sh
eval $(cat vm.config)

multipass launch --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
	--cloud-init ./cloud-init.yaml

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "👋 Initialize ${vm_name}..."

multipass mount workspace ${vm_name}:workspace
multipass mount share ${vm_name}:share

multipass info ${vm_name}

multipass exec ${vm_name}-- sudo -- sh -c "echo \"${IP} ${vm_domain}\" >> /etc/hosts"

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
apt-get update
curl -LO https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh
bash ./script.deb.sh
EXTERNAL_URL='http://${vm_domain}' apt-get install -y gitlab-ee 
EOF


# 🖐 add this to `hosts` file(s)

echo "${IP} ${vm_domain}" > workspace/hosts.config

# 🖐 use this file to exchange data between VM creation script
# use: eval $(cat ../registry/workspace/export.registry.config)
target="workspace/export.gitlab.config"
echo "vm_name=\"${vm_name}\";" >> ${target}
echo "vm_domain=\"${vm_domain}\";" >> ${target}
echo "vm_ip=\"${IP}\";" >> ${target}

