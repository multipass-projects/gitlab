#!/bin/sh
eval $(cat vm.config)

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "👋 ${vm_name}: ${IP}"

# see https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
apt install rpl -y
rpl "external_url 'http://${vm_domain}'" "external_url 'https://${vm_domain}'" /etc/gitlab/gitlab.rb
mkdir -p /etc/gitlab/ssl
chmod 755 /etc/gitlab/ssl
cp share/${vm_domain}.* /etc/gitlab/ssl/
gitlab-ctl reconfigure
EOF


#docker login gl.bots.garden:4567