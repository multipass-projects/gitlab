# GitLab

## Create the VM

- change value of `vm.config`:

  ```bash
  vm_name="glvm";
  vm_domain="gl.bots.garden";
  vm_cpus=4;
  vm_mem="8G";
  vm_disk="100GB";
  ```

- run `./create-vm.sh`
- update your `host` file with the `ip_of_the_vm` and  `domain_of_the_vm` (look at the generated file: `workspace/hosts.config`)
  - for example add this line: `192.168.64.53 gl.bots.garden`

### If you want to activate https

> I use DNS wildcard certificate

- You need certificate and key
- Put certificate and key in `share`
- run `./activate-ssl.sh`

### If you want to activate Docker Registry

- You need to activate https before
- run `./docker-registry.sh`

## Root user

- create root user

> if issue with root user: ref: [https://docs.gitlab.com/ee/security/reset_root_password.html](https://docs.gitlab.com/ee/security/reset_root_password.html)

```bash
sudo gitlab-rails console -e production
user = User.where(id: 1).first
user.password = 'secret_pass'
user.password_confirmation = 'secret_pass'
user.save!
```

## Local network

- go to `http://${your-gitlab-domain-name}/admin/application_settings/network`
- change **Outbound requests**:
  - `Allow requests to the local network from web hooks and services == true`
  - `Allow requests to the local network from system hooks == true`

## host file

Update the host file (of the host machine) with `ip_of_the_vm ip_of_the_vm` (check `workspace` path)