#!/bin/sh
eval $(cat vm.config)

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "👋 ${vm_name}: ${IP}"
# https://docs.gitlab.com/ee/administration/packages/container_registry.html#configure-container-registry-under-an-existing-gitlab-domain
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
apt install rpl -y
rpl "# registry_external_url 'https://registry.example.com'" "registry_external_url 'https://${vm_domain}:4567'" /etc/gitlab/gitlab.rb
gitlab-ctl reconfigure
EOF


